"""init class Animal,Predator and  with attribute"""
from abc import ABCMeta, abstractclassmethod


class SuperAnimal(metaclass=ABCMeta):
    def __init__(self, name_of_animal, type_of_food, amount_of_food=None):
        """
        :param name_of_animal: name_of_animal
        :type name_of_animal: str
        :param amount_of_food: amount_of_food
        :type amount_of_food: int
        :param type_of_food: type_of_food
        :type type_of_food: str
        """
        self.SetNameOfAnimal(name_of_animal)
        self.SetAmountOfFood(amount_of_food)
        self.SetTypeOfFood(type_of_food)

    def SetNameOfAnimal(self, name_of_animal):
        """
        sets name_of_animal
        :param name_of_animal:name_of_animal
        :type name_of_animal: str

        """
        self.__name_of_animal = name_of_animal

    def SetAmountOfFood(self, amount_of_food):
        """
        sets amount_of_food
        :param amount_of_food:amount_of_food
        :type amount_of_food: int

        """

        self.__amount_of_food = amount_of_food

    def SetTypeOfFood(self, type_of_food):
        """
        sets type_of_food
        :param type_of_food:
        :type type_of_food: str

        """
        self.__type_of_food = type_of_food

    def GetNameOfAnimal(self):
        """
        gets name_of_animal
        :return  name_of_animal
        :rtype name_of_animal: str

        """
        return self.__name_of_animal

    def GetAmountOfFood(self):
        """
        gets amount_of_food
        :return  amount_of_food
        :rtype amount_of_food: int

        """
        return self.__amount_of_food

    def GetTypeOfFood(self):
        """
        gets type of food
        :return  type_of_food
        :rtype type_of_food: str

        """
        return self.__type_of_food

    def __str__(self):
        return f"Name of animal: {self.__name_of_animal}\tAmount of food: {self.__amount_of_food}\tType of food: {self.__type_of_food}"

    # @abstractclassmethod
    def count_food(self, multip=1):
        # input("Enter type_of_food:   \n")
        multip = int(input("Enter multip of  amount_of_food:   \n"))
        res = self.__amount_of_food * multip
        return res


class Predator(SuperAnimal):
    def __init__(self, name_of_animal, amount_of_food):
        SuperAnimal.__init__(self, name_of_animal, "meat", amount_of_food)

    def cout_food(self, amount_of_food, type_of_food):
        res = SuperAnimal.count_food(amount_of_food, type_of_food)
        return f"This animal {self.GetNameOfAnimal()} eats {res} of meats"


class Herbivores(SuperAnimal):
    def __init__(self, name_of_animal, amount_of_food):
        SuperAnimal.__init__(self, name_of_animal, "grass", amount_of_food)

    def cout_food(self, amount_of_food, type_of_food):
        res = SuperAnimal.count_food(amount_of_food, type_of_food)
        return f"This animal {self.GetNameOfAnimal()} eats {res} of meats"


class Omnivores(SuperAnimal):
    def __init__(self, name_of_animal, amount_of_food):
        SuperAnimal.__init__(self, name_of_animal, "evereything", amount_of_food)

    def cout_food(self, amount_of_food, type_of_food):
        res = SuperAnimal.count_food(amount_of_food, type_of_food)
        return f"This animal {self.GetNameOfAnimal()} eats {res} of evereything"

# if __name__=="__main__":
#
# d1 = Predator("alex",23)
# print(d1.GetAmountOfFood())
# print(d1.count_food())
# print(d1.GetAmountOfFood())
