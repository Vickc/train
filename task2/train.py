"""init class Train with attribute"""


class Train:
    """
    Информация о поездах :название пункта назначения, номер поезда, время
    отправления. возможность вывода информации о поезде, номер которого
    введен пользователем.
    """

    def __init__(self, place, number, time):
        """
        :param place: name of place
        :type place: str
        :param number: number of trip
        :type number: int
        :param time: time of leaving
        :type time: int
        """
        res = []
        self.SetPlace(place)
        self.SetNumber(number)
        self.SetTime(time)

    def SetPlace(self, place):
        """
        set place of trip
        :param place: name of place
        :type place: str
        """

        self.__place = place

    def SetNumber(self, number):
        """
        set number of trip
        :param number: number of trip
        :type number: int
        """

        self.__number = number

    def SetTime(self, time):
        """
        set time of leaving
        :param time: time
        :type time: int
        """

        self.__time = time

    def GetPlace(self):
        """
        get place of trip
        :return:place of trip
        :rtype: str
        """

        return self.__place

    def GetNumber(self):
        """
        get number of trip
        :return: number of train
        :rtype: int
        """

        return self.__number

    def GetTime(self):
        """
        get time of leaving
        :return: time of leaving
        :rtype: int
        """

        return self.__time

    def __str__(self):
        return f"number: {self.__number}\tPlace: {self.__place}\tTime: {self.__time}"
